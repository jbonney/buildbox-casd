/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_CASSERVER_H
#define INCLUDED_BUILDBOXCASD_CASSERVER_H

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_context.h>

#include <buildboxcommon_protos.h>

#include <memory>
#include <regex>

#include <buildboxcasd_casinstance.h>
#include <buildboxcasd_localcas.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;
using namespace google::protobuf::util;
using namespace google::protobuf;
using namespace google::bytestream;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

class CasRemoteExecutionServicer final
    : public ContentAddressableStorage::Service {
    /* Implements the CAS RPC methods. */

  public:
    explicit CasRemoteExecutionServicer(std::shared_ptr<CasInstance> cas);

    Status FindMissingBlobs(ServerContext *context,
                            const FindMissingBlobsRequest *request,
                            FindMissingBlobsResponse *response) override;

    Status BatchUpdateBlobs(ServerContext *context,
                            const BatchUpdateBlobsRequest *request,
                            BatchUpdateBlobsResponse *response) override;

    Status BatchReadBlobs(ServerContext *context,
                          const BatchReadBlobsRequest *request,
                          BatchReadBlobsResponse *response) override;

    Status GetTree(ServerContext *context, const GetTreeRequest *request,
                   ServerWriter<GetTreeResponse> *writer) override;

  private:
    std::shared_ptr<CasInstance> d_cas;
};

class CasBytestreamServicer final : public ByteStream::Service {
    /* Implements the Bytestream API Read() and Write() methods for the CAS. */
  public:
    explicit CasBytestreamServicer(std::shared_ptr<CasInstance> cas);

    Status Read(grpc::ServerContext *context, const ReadRequest *request,
                ServerWriter<ReadResponse> *writer) override;

    Status Write(grpc::ServerContext *context,
                 ServerReader<WriteRequest> *request,
                 WriteResponse *response) override;

  private:
    std::shared_ptr<CasInstance> d_cas;
};

class CasService {
    /* Class that provides the `ContentAddressableStorage` methods specified
     * by the remote execution API.
     * The implementation is split into two servicers, one employing
     * the Bytestream API.
     */
  public:
    CasService(std::shared_ptr<LocalCas> cas_storage);

    CasService(std::shared_ptr<LocalCas> cas_storage,
               std::shared_ptr<buildboxcommon::Client> cas_client);

    inline std::shared_ptr<grpc::Service> remoteExecutionCasServicer()
    {
        return d_remote_execution_cas_servicer;
    }
    inline std::shared_ptr<grpc::Service> bytestreamServicer()
    {
        return d_cas_bytestream_servicer;
    }

  private:
    std::shared_ptr<LocalCas> d_cas_storage;
    std::shared_ptr<CasInstance> d_cas;

    const std::shared_ptr<CasRemoteExecutionServicer>
        d_remote_execution_cas_servicer;
    const std::shared_ptr<CasBytestreamServicer> d_cas_bytestream_servicer;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_CASSERVER_H
