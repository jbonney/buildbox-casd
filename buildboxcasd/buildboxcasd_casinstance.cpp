/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casinstance.h>
#include <buildboxcommon_logging.h>

using namespace buildboxcasd;

grpc::Status CasInstance::Read(const ReadRequest &request,
                               ServerWriter<ReadResponse> *writer)
{
    const std::string resource_name = request.resource_name();

    BUILDBOX_LOG_DEBUG("Bytestream.Read(" << resource_name << ")");

    Digest digest;
    const grpc::Status argument_status =
        bytestreamReadArgumentStatus(request, &digest);

    if (!argument_status.ok()) {
        return argument_status;
    }

    const auto read_limit = request.read_limit();
    const auto read_offset = request.read_offset();

    std::string data;
    const auto status =
        readBlob(digest, &data, static_cast<size_t>(read_offset),
                 static_cast<size_t>(read_limit));

    if (status.code() != grpc::StatusCode::OK) {
        return grpc::Status(static_cast<grpc::StatusCode>(status.code()),
                            status.message());
    }

    // Returning the requested data:
    ReadResponse response;
    response.set_data(data.data());
    writer->Write(response);

    return grpc::Status::OK;
}

grpc::Status CasInstance::Write(ServerReader<WriteRequest> &request,
                                WriteResponse *response)
{
    WriteRequest request_message;
    request.Read(&request_message);

    BUILDBOX_LOG_DEBUG("Bytestream.Write(" << request_message.resource_name()
                                           << ")");

    Digest digest;
    const grpc::Status argument_status =
        bytestreamWriteArgumentStatus(request_message, &digest);

    if (!argument_status.ok()) {
        return argument_status;
    }

    if (hasBlob(digest)) {
        return grpc::Status(grpc::StatusCode::ALREADY_EXISTS,
                            "Blob already exists in the local CAS.");
    }

    // We do *not* support partial writes yet (!)
    if (request_message.write_offset() != 0 ||
        !request_message.finish_write()) {
        return grpc::Status(grpc::StatusCode::UNIMPLEMENTED,
                            "Partial writes are not supported yet");
    }

    const google::rpc::Status write_status =
        writeBlob(digest, request_message.data());

    if (write_status.code() == grpc::StatusCode::OK) {
        response->set_committed_size(digest.size_bytes());
    }

    return grpc::Status(static_cast<grpc::StatusCode>(write_status.code()),
                        write_status.message());
}

grpc::Status CasInstance::GetTree(const GetTreeRequest &request,
                                  ServerWriter<GetTreeResponse> *writer)
{
    // TODO: Implement pagination
    GetTreeResponse response;

    const Digest root_digest = request.root_digest();
    BUILDBOX_LOG_DEBUG("GetTree(" << root_digest.hash() << ")");

    try {
        const Directory root_directory = getDirectory(root_digest);

        buildTree(root_directory, response);

        writer->Write(response);
        return grpc::Status::OK;
    }
    catch (const BlobNotFoundException &) {
        return grpc::Status(grpc::StatusCode::NOT_FOUND,
                            "The root digest was not found in the local CAS.");
    }
    catch (const std::runtime_error &e) {
        BUILDBOX_LOG_ERROR("GetTree(): Error building tree: " << e.what());
    }

    return grpc::Status(grpc::StatusCode::INTERNAL,
                        "Could not build the requested tree.");
}

Directory CasInstance::getDirectory(const Digest &digest)
{
    std::string subdirectory_blob;

    const google::rpc::Status read_status =
        readBlob(digest, &subdirectory_blob, 0, 0);

    if (read_status.code() == grpc::StatusCode::OK) {
        Directory d;
        if (d.ParseFromString(subdirectory_blob)) {
            return d;
        }
        else {
            BUILDBOX_LOG_DEBUG(digest.hash() << "/" << digest.size_bytes()
                                             << ": error parsing Directory.");
        }
    }
    throw BlobNotFoundException(
        "Directory blob could not be read from local CAS.");
}

void CasInstance::buildTree(const Directory &directory,
                            GetTreeResponse &response)
{
    // Adding the directory to the response:
    Directory *directory_entry = response.add_directories();
    directory_entry->CopyFrom(directory);

    // And recursively adding all its subdirectories to the response...
    for (const DirectoryNode &subdirectory_node : directory.directories()) {
        // (sub)DirectoryNode -> directory_node (blob) -> Directory proto
        const Digest subdirectory_digest = subdirectory_node.digest();
        try {
            const Directory subdirectory_proto =
                getDirectory(subdirectory_digest);
            buildTree(subdirectory_proto, response);
        }
        catch (const BlobNotFoundException &) {
            ;
        }
        // (According to the RE specification, if some portion of the tree
        // is missing from the CAS, we still have to return what we have. So we
        // just skip missing subdirectories.)
    }
}

LocalCasInstance::LocalCasInstance(std::shared_ptr<LocalCas> storage)
    : d_storage(storage)
{
}

grpc::Status
LocalCasInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                   FindMissingBlobsResponse *response)
{
    // Returns a list of digests that are *not* in the CAS.
    BUILDBOX_LOG_DEBUG("FindMissingBlobs()");

    for (const Digest &digest : request.blob_digests()) {
        bool blob_in_cas = false;

        try {
            blob_in_cas = d_storage->hasBlob(digest);
        }
        catch (const std::runtime_error &) {
            BUILDBOX_LOG_ERROR("Could not determine if "
                               << digest.hash() << "is in local CAS.");
        }

        if (!blob_in_cas) {
            Digest *entry = response->add_missing_blob_digests();
            entry->CopyFrom(digest);
        }
    }
    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                   BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_DEBUG("BatchUpdateBlobs(): " << request.requests_size()
                                              << " blobs");
    for (const auto &blob : request.requests()) {
        const google::rpc::Status status =
            writeToLocalStorage(blob.digest(), blob.data());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                 BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_DEBUG("BatchReadBlobs(): " << request.digests_size()
                                            << " digests");
    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status status = readFromLocalStorage(digest, &data);

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);
        entry->mutable_status()->CopyFrom(status);
        entry->set_data(data);
    }
    return grpc::Status::OK;
}

/*
 * Helpers to parse resource name URLs and extract digests
 */
Digest
CasInstance::digestFromDownloadResourceName(const std::string &resource_name)
{
    /* Checks that the resource name has the form:
     * "{instance_name}/blobs/{hash}/{size}",
     * and attempts to parse the hash and size and return a Digest object.
     * If not, raise an invalid argument exception.
     */
    static const std::regex regex("^(?:\\w+/)?blobs/(.+)/(\\d+)");
    return digestFromResourceName(resource_name, regex);
}

Digest
CasInstance::digestFromUploadResourceName(const std::string &resource_name)
{
    /* Checks that the resource name has the form:
     * "{instance_name}/uploads/{uuid}/blobs/{hash}/{size}",
     * and attempts to parse the hash and size and return a Digest object.
     * If not, raise an invalid argument exception.
     */
    static const std::regex regex("^(?:\\w+/)?uploads/.+/blobs/(.+)/(\\d+)");
    return digestFromResourceName(resource_name, regex);
}

Digest CasInstance::digestFromResourceName(const std::string &resource_name,
                                           const std::regex &regex)
{
    std::smatch matches;

    if (std::regex_search(resource_name, matches, regex) &&
        matches.size() == 3) {
        const std::string hash = matches[1];
        const std::string size = matches[2];

        Digest d;
        d.set_hash(hash);
        d.set_size_bytes(std::stoi(size));
        return d;
    }

    throw std::invalid_argument("Resource name \"" + resource_name + "\" " +
                                "is not valid");
}

LocalCasProxyInstance::LocalCasProxyInstance(
    std::shared_ptr<LocalCas> storage,
    std::shared_ptr<buildboxcommon::Client> cas_client)
    : LocalCasInstance(storage), d_cas_client(cas_client)
{
}

grpc::Status
LocalCasProxyInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                        FindMissingBlobsResponse *response)
{
    BUILDBOX_LOG_DEBUG("FindMissingBlobs()");
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request.blob_digests()) {
        if (!d_storage->hasBlob(digest)) {
            digests_missing_locally.push_back(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        return grpc::Status::OK;
    }

    const std::vector<Digest> digests_missing_in_remote =
        d_cas_client->findMissingBlobs(digests_missing_locally);

    for (const Digest &digest : digests_missing_in_remote) {
        Digest *entry = response->add_missing_blob_digests();
        entry->CopyFrom(digest);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                        BatchUpdateBlobsResponse *response)
{

    BUILDBOX_LOG_DEBUG("BatchUpdateBlobs()");
    // TODO: Issue a FindMissingBlobs request first to avoid sending
    // duplicates.
    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    for (const auto &blob : request.requests()) {
        buildboxcommon::Client::UploadRequest upload_request(blob.digest(),
                                                             blob.data());
        upload_requests.push_back(upload_request);

        writeToLocalStorage(blob.digest(), blob.data());
    }

    const std::set<std::string> digests_not_uploaded =
        batchUpdateRemoteCas(request);
    for (const auto &blob : request.requests()) {
        google::rpc::Status status;
        if (digests_not_uploaded.count(blob.digest().hash())) {
            status.set_code(grpc::StatusCode::UNAVAILABLE);
            status.set_message("Could not update blob to remote CAS");
        }
        else {
            status.set_code(grpc::StatusCode::OK);
        }

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                      BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_DEBUG("BatchReadBlobs()");
    // Checking the local storage first:
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status read_status =
            readFromLocalStorage(digest, &data);

        if (read_status.code() == grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(read_status);
            entry->set_data(data);
        }
        else {
            digests_missing_locally.push_back(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        return grpc::Status::OK;
    }

    // Making a request for the blobs that we couldn't find locally:
    const buildboxcommon::Client::DownloadedData downloaded_data =
        d_cas_client->downloadBlobs(digests_missing_locally);

    for (const Digest &digest : digests_missing_locally) {
        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);

        google::rpc::Status status;

        const auto digest_data_it = downloaded_data.find(digest.hash());
        if (digest_data_it != downloaded_data.cend()) {
            const std::string data = digest_data_it->second;
            status.set_code(grpc::StatusCode::OK);
            entry->set_data(data);

            writeToLocalStorage(digest, data);
        }
        else {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob could not be fetched from remote CAS");
        }

        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

bool LocalCasProxyInstance::hasBlob(const Digest &digest)
{
    try {
        if (!d_storage->hasBlob(digest)) {
            const std::string directory_entry =
                d_cas_client->fetchString(digest);
            d_storage->writeBlob(digest, directory_entry);
        }
        return true;
    }
    catch (const std::runtime_error &) {
        return false;
    }
}

google::rpc::Status LocalCasProxyInstance::readBlob(const Digest &digest,
                                                    std::string *data,
                                                    size_t read_offset,
                                                    size_t read_limit)
{
    const google::rpc::Status read_status =
        readFromLocalStorage(digest, data, read_offset, read_limit);
    if (read_status.code() == grpc::StatusCode::OK) {
        return read_status;
    }

    google::rpc::Status status;
    try {
        const std::string fetched_data = d_cas_client->fetchString(digest);

        if (read_limit == 0) {
            *data = fetched_data.substr(read_offset);
        }
        else {
            *data = fetched_data.substr(read_offset, read_limit);
        }

        status.set_code(grpc::StatusCode::OK);

        writeToLocalStorage(digest, fetched_data);
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in local nor remote CAS");
    }

    return status;
}

google::rpc::Status LocalCasProxyInstance::writeBlob(const Digest &digest,
                                                     const std::string &data)
{
    google::rpc::Status status;

    // Trying to upload the blob first:
    try {
        d_cas_client->upload(data, digest);
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::UNAVAILABLE);
        return status;
    }

    // Upload was successful. We now write to the local storage
    // (if needed, LocalCas will skip the write if the file exists.):
    google::rpc::Status write_status = writeToLocalStorage(digest, data);
    status.set_code(write_status.code());

    return status;
}

std::set<std::string> LocalCasProxyInstance::batchUpdateRemoteCas(
    const BatchUpdateBlobsRequest &request)
{
    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    for (const auto &blob : request.requests()) {
        buildboxcommon::Client::UploadRequest upload_request(blob.digest(),
                                                             blob.data());
        upload_requests.push_back(upload_request);
    }

    const buildboxcommon::Client::UploadResults uploaded_blobs =
        d_cas_client->uploadBlobs(upload_requests);

    std::set<std::string> res;
    for (const Digest &digest : uploaded_blobs) {
        res.insert(digest.hash());
    }

    return res;
}

google::rpc::Status
LocalCasInstance::writeToLocalStorage(const Digest &digest,
                                      const std::string data)
{
    google::rpc::Status status;

    try {
        d_storage->writeBlob(digest, data);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const std::invalid_argument &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message(
            "The size of the data does not match the size defined "
            "in the digest.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message("Internal error while writing blob to local CAS: " +
                           std::string(e.what()));
    }

    return status;
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest,
                                       std::string *data) const
{
    return readFromLocalStorage(digest, data, 0, 0);
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest, std::string *data,
                                       size_t offset, size_t limit) const
{
    google::rpc::Status status;

    try {
        const size_t read_length = (limit > 0) ? limit : LocalCas::npos;
        *data = d_storage->readBlob(digest, offset, read_length);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const BlobNotFoundException &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in the local CAS.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message(
            "Internal error while fetching blob in local CAS: " +
            std::string(e.what()));
    }

    return status;
}

bool LocalCasInstance::hasBlob(const Digest &digest)
{
    return d_storage->hasBlob(digest);
}

google::rpc::Status LocalCasInstance::readBlob(const Digest &digest,
                                               std::string *data,
                                               size_t read_offset,
                                               size_t read_limit)
{
    return readFromLocalStorage(digest, data, read_offset, read_limit);
}

google::rpc::Status LocalCasInstance::writeBlob(const Digest &digest,
                                                const std::string &data)
{
    return writeToLocalStorage(digest, data);
}

grpc::Status
CasInstance::bytestreamReadArgumentStatus(const ReadRequest &request,
                                          Digest *digest)
{
    const auto read_limit = request.read_limit();
    if (read_limit < 0) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "`read_limit` cannot be negative.");
    }

    const auto read_offset = request.read_offset();
    if (read_offset < 0) {
        return grpc::Status(grpc::StatusCode::OUT_OF_RANGE,
                            "`read_offset` cannot be negative.");
    }

    // Checking that the resource name of the request is pertinent and if so
    // extracting the Digest value from it:
    const std::string resource_name = request.resource_name();
    try {
        const Digest requested_digest =
            digestFromDownloadResourceName(resource_name);

        if (read_offset > requested_digest.size_bytes()) {
            return grpc::Status(
                grpc::StatusCode::OUT_OF_RANGE,
                "`read_offset` cannot be larger than the size of the data: " +
                    std::to_string(read_offset) + " > " +
                    std::to_string(requested_digest.size_bytes()));
        }

        digest->CopyFrom(requested_digest);
        return grpc::Status::OK;
    }
    catch (const std::invalid_argument &) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "The requested resource (\"" + resource_name +
                                "\") is not valid.");
    }
}

grpc::Status
CasInstance::bytestreamWriteArgumentStatus(const WriteRequest &request,
                                           Digest *digest)
{
    try {
        const Digest d = digestFromUploadResourceName(request.resource_name());

        const auto digest_size_bytes = static_cast<size_t>(d.size_bytes());
        if (digest_size_bytes != request.data().size()) {
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                                "Size of the data does not match the size "
                                "reported in the digest");
        }

        *digest = d;
        return grpc::Status::OK;
    }
    catch (const std::invalid_argument &) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "The requested resource is not valid.");
    }
}
