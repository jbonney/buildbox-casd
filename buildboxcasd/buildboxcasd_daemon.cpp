/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_daemon.h>

#include <exception>
#include <fcntl.h>
#include <google/rpc/code.pb.h>
#include <grpcpp/channel.h>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <system_error>
#include <thread>
#include <unistd.h>

#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

#include <buildboxcommon_client.h>
#include <buildboxcommon_logging.h>

#include <buildboxcasd_casserver.h>

namespace buildboxcasd {

void Daemon::runDaemon()
{
    BUILDBOX_LOG_SET_LEVEL(buildboxcommon::LogLevel::DEBUG);

    BUILDBOX_LOG_INFO("Starting CASd with cache at "
                      << this->d_localCachePath);
    auto storage = std::make_shared<FsLocalCas>(this->d_localCachePath);

    std::shared_ptr<CasService> cas_server;
    if (d_casServer.d_url) { // CAS proxy
        BUILDBOX_LOG_INFO("Creating CAS proxy server. Remote CAS address: "
                          << d_casServer.d_url);
        auto remote_cas_client = std::make_shared<buildboxcommon::Client>();
        remote_cas_client->init(d_casServer);
        cas_server = std::make_shared<CasService>(storage, remote_cas_client);
    }
    else { // Local CAS server
        BUILDBOX_LOG_DEBUG("Creating LocalCAS server");
        cas_server = std::make_shared<CasService>(storage);
    }

    grpc::ServerBuilder builder;

    if (d_bind_address.empty()) {
        d_bind_address = "unix:" + d_localCachePath + "/casd.sock";
    }

    builder.AddListeningPort(d_bind_address,
                             grpc::InsecureServerCredentials());
    builder.RegisterService(cas_server->remoteExecutionCasServicer().get());
    builder.RegisterService(cas_server->bytestreamServicer().get());

    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    BUILDBOX_LOG_INFO("Server listening on " << d_bind_address);
    server->Wait();
}

} // namespace buildboxcasd
