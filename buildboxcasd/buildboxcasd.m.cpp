/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_daemon.h>

#include <buildboxcommon_connectionoptions.h>

#include <cstring>
#include <iostream>
#include <string>
#include <unistd.h>

using namespace buildboxcommon;
using namespace buildboxcasd;

static void usage(const char *name)
{
    std::cerr << "usage: " << name << " [OPTIONS] LOCAL_CACHE\n";
    std::cerr << "    --instance=INSTANCE         Pass an instance name to "
                 "the server\n";
    ConnectionOptions::printArgHelp(32, "CAS", "cas-");
    std::cerr << "    --bind                      Bind to \"address:port\" or "
                 "UNIX socket in \n"
              << "                                \"unix:path\".\n"
              << "                                (Default: "
                 "unix:LOCAL_CACHE/casd.sock)\n";
    std::cerr << "    --verbose                   Enable verbose logging\n";
}

int main(int argc, char *argv[])
{
    const char *program_name = argv[0];
    argv++;
    argc--;

    Daemon daemon;

    while (argc > 0) {
        const char *arg = argv[0];
        const char *assign = strchr(arg, '=');
        if (daemon.d_casServer.parseArg(arg, "cas-")) {
            // Argument was handled by server_opts.
        }
        else if (arg[0] == '-' && arg[1] == '-') {
            arg += 2;
            if (assign) {
                const int key_len = assign - arg;
                const char *value = assign + 1;
                if (strncmp(arg, "instance", key_len) == 0) {
                    daemon.d_instance = value;
                }
                else if (strncmp(arg, "bind", key_len) == 0) {
                    daemon.d_bind_address = value;
                }
                else {
                    std::cerr << "Invalid option " << argv[0] << "\n";
                    usage(program_name);
                    return 1;
                }
            }
            else {
                if (strcmp(arg, "help") == 0) {
                    usage(program_name);
                    return 0;
                }
                else if (strcmp(arg, "verbose") == 0) {
                    daemon.d_verbose = true;
                }
                else {
                    std::cerr << "Invalid option " << argv[0] << "\n";
                    usage(program_name);
                    return 1;
                }
            }
        }
        else if (daemon.d_localCachePath.empty()) {
            daemon.d_localCachePath = arg;
        }
        else {
            std::cerr << "Unexpected argument " << arg << "\n";
            usage(program_name);
            return 1;
        }
        argv++;
        argc--;
    }

    if (daemon.d_localCachePath.empty()) {
        std::cerr << "Local cache path is missing\n";
        usage(program_name);
        return 1;
    }

    daemon.runDaemon();
    return 0;
}
