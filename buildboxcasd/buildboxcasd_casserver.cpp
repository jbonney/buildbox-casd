/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casserver.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;
using namespace google::bytestream;
using namespace google::protobuf::util;
using namespace google::protobuf;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

CasService::CasService(std::shared_ptr<LocalCas> cas_storage)
    : d_cas_storage(cas_storage),
      d_cas(std::make_shared<LocalCasInstance>(d_cas_storage)),
      d_remote_execution_cas_servicer(
          std::make_shared<CasRemoteExecutionServicer>(d_cas)),
      d_cas_bytestream_servicer(std::make_shared<CasBytestreamServicer>(d_cas))
{
}

CasService::CasService(std::shared_ptr<LocalCas> cas_storage,
                       std::shared_ptr<buildboxcommon::Client> cas_client)
    : d_cas_storage(cas_storage),
      d_cas(
          std::make_shared<LocalCasProxyInstance>(d_cas_storage, cas_client)),
      d_remote_execution_cas_servicer(
          std::make_shared<CasRemoteExecutionServicer>(d_cas)),
      d_cas_bytestream_servicer(std::make_shared<CasBytestreamServicer>(d_cas))
{
}

/*
 * Remote Execution API: Content Addressable Storage service
 *
 */
CasRemoteExecutionServicer::CasRemoteExecutionServicer(
    std::shared_ptr<CasInstance> cas)
    : d_cas(cas)
{
}

Status CasRemoteExecutionServicer::FindMissingBlobs(
    ServerContext *context, const FindMissingBlobsRequest *request,
    FindMissingBlobsResponse *response)
{
    // Returns a list of digests that are *not* in the CAS.
    return d_cas->FindMissingBlobs(*request, response);
}

Status CasRemoteExecutionServicer::BatchUpdateBlobs(
    ServerContext *context, const BatchUpdateBlobsRequest *request,
    BatchUpdateBlobsResponse *response)
{
    return d_cas->BatchUpdateBlobs(*request, response);
}

Status CasRemoteExecutionServicer::BatchReadBlobs(
    ServerContext *context, const BatchReadBlobsRequest *request,
    BatchReadBlobsResponse *response)
{
    return d_cas->BatchReadBlobs(*request, response);
}

Status
CasRemoteExecutionServicer::GetTree(ServerContext *context,
                                    const GetTreeRequest *request,
                                    ServerWriter<GetTreeResponse> *stream)
{
    return d_cas->GetTree(*request, stream);
}

/*
 * Bytestream API
 *
 * We will service resource names that look like:
 *    "{instance_name}/uploads/{uuid}/blobs/{hash}/{size}", or
 *    "{instance_name}/blobs/{hash}/{size}"
 *  with anything after "size" being ignored.
 */
CasBytestreamServicer::CasBytestreamServicer(std::shared_ptr<CasInstance> cas)
    : d_cas(cas)
{
}

Status CasBytestreamServicer::Read(ServerContext *context,
                                   const ReadRequest *request,
                                   ServerWriter<ReadResponse> *writer)
{
    return d_cas->Read(*request, writer);
}

Status CasBytestreamServicer::Write(ServerContext *context,
                                    ServerReader<WriteRequest> *request,
                                    WriteResponse *response)
{
    return d_cas->Write(*request, response);
}

} // namespace buildboxcasd
