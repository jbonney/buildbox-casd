/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALCAS_H
#define INCLUDED_BUILDBOXCASD_LOCALCAS_H

#include <buildboxcommon_client.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {

using build::bazel::remote::execution::v2::Digest;

class LocalCas {
  public:
    // Returns whether the given Digest is stored in the local CAS.
    virtual bool hasBlob(const Digest &digest) = 0;

    /* Saves a blob in the storage and returns whether it had to actually
     * perform the write operation (i.e. if the file exists, it returns false.)
     *
     * In cases where the size reported in the Digest does not match the actual
     * size of the data, it raises an `std::invalid_argument` exception.
     *
     * If the write operation fails for another reason, it throws a
     * `std::runtime_error` exception.
     */
    virtual void writeBlob(const Digest &digest, const std::string &data) = 0;

    /* Returns the corresponding data for a digest stored in the local CAS.
     *
     * If the optional parameter `limit` is set to
     * `buildboxcasd::LocalCas::npos`, it reads from `offset` until the end of
     * the data.
     *
     *
     * Throws:
     *  - `BlobNotFoundException` if the digest is not present,
     *  - `std::out_of_range` if `offset` is larger than the data size, or
     *  - `std::runtime_error` if the read fails.
     */
    virtual std::string readBlob(const Digest &digest) = 0;

    virtual std::string readBlob(const Digest &digest, size_t offset,
                                 size_t length) = 0;

    virtual std::string path(const Digest &digest) = 0;

    static const size_t npos = std::string::npos;
};

class FsLocalCas : public LocalCas {
    /* Implements a local, content-addressable storage in which the blobs are
     * stored in a tree directory structure indexed by their Digests.
     *
     * The first two most significant characters of the hashes are used
     * for a top-level directory name and the rest as a filename.
     *
     * For example, the contents of a blob with Digest
     * `ec75218cfaaebdd03d1c135c96c7576a323ffb7bb63bb456a8e6da5bd5dbc612/58`
     * would be stored in:
     * d_storage_root/cas/objects/ec/
     *   |- 75218cfaaebdd03d1c135c96c7576a323ffb7bb63bb456a8e6da5bd5dbc612
     */
  public:
    // Create a new LocalCAS using the given directory to store files:
    FsLocalCas(const std::string &root_path);

    bool hasBlob(const Digest &digest) override;

    void writeBlob(const Digest &digest, const std::string &data) override;

    std::string readBlob(const Digest &digest, size_t offset,
                         size_t length) override;

    std::string readBlob(const Digest &digest) override;

    std::string path(const Digest &digest) override;

    static int pathHashPrefixLength() { return k_HASH_PREFIX_LENGTH; };

  private:
    const std::string d_storage_root;
    const std::string d_objects_directory;
    const std::string d_temp_directory;

    /* Given a Digest, returns the corresponding path to the store content in
     * the storage.
     *
     * The `create_parent_directory` parameter, if set, ensures that the top
     * level directory will be present in the CAS `objects/` directory.
     *
     * For example, for digest "xy...abc/123", directory `cas/objects/xy` will
     * be created and the path returned:
     * "d_storage_root/cas/objects/xy/...abc".
     */
    std::string filePath(const Digest &digest,
                         bool create_parent_directory = false) const;

    /* Given a Digest and some data, checks that the digest matches the actual
     * data.
     *
     * On success it returns. Otherwise it throws an `std::invalid_argument`
     * exception.
     */
    void validateDigest(const Digest &digest, const std::string &data) const;

    /* Given a Digest, attempts to open its corresponding file.
     * If the file does not exist, throws a `BlobNotFoundException`.
     */
    std::ifstream openFile(const Digest &digest);

    // Number of most significant hash characters to take for top-level
    // directory names:
    static const int k_HASH_PREFIX_LENGTH = 2;
};

class BlobNotFoundException : public std::exception {
  public:
    explicit BlobNotFoundException(const char *message) : d_message(message) {}
    explicit BlobNotFoundException(const std::string &message)
        : d_message(message)
    {
    }

    const char *what() const noexcept override { return d_message.c_str(); }

  private:
    std::string d_message;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALCAS_H
