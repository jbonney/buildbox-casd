#ifndef INCLUDED_BUILDBOXCASD_CAS_H
#define INCLUDED_BUILDBOXCASD_CAS_H

#include <buildboxcasd_localcas.h>
#include <buildboxcommon_protos.h>

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <regex>

using namespace build::bazel::remote::execution::v2;
using namespace google::bytestream;
using namespace google::protobuf::util;
using namespace google::protobuf;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

namespace buildboxcasd {

class CasInstance {
    /* Defines a common interface for providing the necessary methods to
     * service the calls of the CAS service portion of the Remote Execution
     * API.
     *
     * We distinguish between the methods that operate on multiple
     * blobs (those are defined as virtual) and those that operate on a single
     * blob (implemented here and shared among different specializations of
     * this class). The goal is to allow sharing as much common code as
     * possible between implementations.
     */
  public:
    virtual grpc::Status
    FindMissingBlobs(const FindMissingBlobsRequest &request,
                     FindMissingBlobsResponse *response) = 0;

    virtual grpc::Status
    BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                     BatchUpdateBlobsResponse *response) = 0;

    virtual grpc::Status BatchReadBlobs(const BatchReadBlobsRequest &request,
                                        BatchReadBlobsResponse *response) = 0;

    // These calls can be shared among LocalCAS and CAS proxy instances since
    // they process individual blobs:
    grpc::Status GetTree(const GetTreeRequest &request,
                         ServerWriter<GetTreeResponse> *writer);

    grpc::Status Write(ServerReader<WriteRequest> &request,
                       WriteResponse *response);

    grpc::Status Read(const ReadRequest &request,
                      ServerWriter<ReadResponse> *writer);

  private:
    /* Parses a `resource_name` string and extracts the Digest it specifies.
     * If the resource name is not relevant to the Bytestream CAS requests
     * that we are servicing, or is invalid, it throws an
     * `std::invalid_argument` exception.
     */
    static Digest
    digestFromUploadResourceName(const std::string &resource_name);
    static Digest
    digestFromDownloadResourceName(const std::string &resource_name);
    static Digest digestFromResourceName(const std::string &resource_name,
                                         const std::regex &regex);

    void buildTree(const Directory &directory, GetTreeResponse &response);

    /* These functions allow the `buildTree()` algorithm and the
     * ByteStream operations to be shared among the different
     * `ContentAddresableStorageImplementation` instances.
     */
    virtual bool hasBlob(const Digest &digest) = 0;

    virtual google::rpc::Status readBlob(const Digest &digest,
                                         std::string *data, size_t read_offset,
                                         size_t read_limit) = 0;

    virtual google::rpc::Status writeBlob(const Digest &digest,
                                          const std::string &data) = 0;

    /* Given a digest returns a `Directory` object.
     * Precondition: `hasBlob(digest) == true` (which is how `buildTree()`
     * uses this function.)
     */
    Directory getDirectory(const Digest &digest);

    /* Helpers to parse the different arguments provided to the Bytestream
     * calls.
     *
     * If the values are in range and the digest could be parsed properly,
     * it is written into `digest`. In case any error is encountered, `digest`
     * is not modified.
     */
    static grpc::Status
    bytestreamReadArgumentStatus(const ReadRequest &request, Digest *digest);

    static grpc::Status
    bytestreamWriteArgumentStatus(const WriteRequest &request, Digest *digest);
};

class LocalCasInstance
    /* This class implements the logic for methods that service a CAS server,
     * as defined by the Remote Execution API.
     */
    : public CasInstance {
  public:
    explicit LocalCasInstance(std::shared_ptr<LocalCas> storage);

    grpc::Status FindMissingBlobs(const FindMissingBlobsRequest &request,
                                  FindMissingBlobsResponse *response);

    grpc::Status BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                  BatchUpdateBlobsResponse *response);

    grpc::Status BatchReadBlobs(const BatchReadBlobsRequest &request,
                                BatchReadBlobsResponse *response);

  protected:
    /* Writes to the local storage, returning the status code that should be
     * attached to the response to the client.
     */
    google::rpc::Status writeToLocalStorage(const Digest &digest,
                                            const std::string data);

    /* Attemps to read from the local storage into `data` returning a gRPC
     * status code that can be embeded into a reply.
     *
     * If the status returned is not successful, `data` remains unmodified.
     */
    google::rpc::Status readFromLocalStorage(const Digest &digest,
                                             std::string *data) const;

    google::rpc::Status readFromLocalStorage(const Digest &digest,
                                             std::string *data, size_t offset,
                                             size_t limit) const;

    std::shared_ptr<LocalCas> d_storage;

  private:
    bool hasBlob(const Digest &digest) override;

    google::rpc::Status readBlob(const Digest &digest, std::string *data,
                                 size_t read_offset, size_t read_limit);

    google::rpc::Status writeBlob(const Digest &digest,
                                  const std::string &data);
};

class LocalCasProxyInstance : public LocalCasInstance {
    /* This class implements the logic for methods that service a CAS server
     * that also acts as a proxy pointing to a remote one.
     *
     * Inheriting from `LocalContentAddresableStorageInstance` avoids having
     * to reimplement behavior that is shared with that type of server.
     */
  public:
    explicit LocalCasProxyInstance(
        std::shared_ptr<LocalCas> storage,
        std::shared_ptr<buildboxcommon::Client> cas_client);

    grpc::Status FindMissingBlobs(const FindMissingBlobsRequest &request,
                                  FindMissingBlobsResponse *response) override;

    grpc::Status BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                  BatchUpdateBlobsResponse *response) override;

    grpc::Status BatchReadBlobs(const BatchReadBlobsRequest &request,
                                BatchReadBlobsResponse *response) override;

  private:
    std::shared_ptr<buildboxcommon::Client> d_cas_client;

    std::set<std::string>
    batchUpdateRemoteCas(const BatchUpdateBlobsRequest &request);

    /* Returns whether the given digest is available to be read from the
     * LocalCAS.
     *
     * If the digest is not stored locally, it will attempt to download it
     * from the remote CAS. If that fails, returns `false`.
     */
    bool hasBlob(const Digest &digest) override;

    google::rpc::Status readBlob(const Digest &digest, std::string *data,
                                 size_t read_offset,
                                 size_t read_limit) override;

    google::rpc::Status writeBlob(const Digest &digest,
                                  const std::string &data) override;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_CAS_H
