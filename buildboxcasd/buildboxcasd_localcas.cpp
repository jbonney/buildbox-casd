/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcas.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <pthread.h>
#include <sstream>
#include <stdexcept>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_temporaryfile.h>

using namespace buildboxcasd;

FsLocalCas::FsLocalCas(const std::string &root_path)
    : d_storage_root(root_path + "/cas"),
      d_objects_directory(d_storage_root + "/objects"),
      d_temp_directory(d_storage_root + "/tmp")
{
    BUILDBOX_LOG_INFO("Creating LocalCAS in " << d_storage_root);

    try {
        BUILDBOX_LOG_DEBUG("Creating root directory: " << root_path);
        buildboxcommon::FileUtils::create_directory(root_path.c_str());

        BUILDBOX_LOG_DEBUG("Creating cas directory: " << d_storage_root)
        buildboxcommon::FileUtils::create_directory(d_storage_root.c_str());

        BUILDBOX_LOG_DEBUG(
            "Creating objects directory: " << d_objects_directory);
        buildboxcommon::FileUtils::create_directory(
            d_objects_directory.c_str());

        BUILDBOX_LOG_DEBUG("Creating temp directory: " << d_temp_directory)
        buildboxcommon::FileUtils::create_directory(d_temp_directory.c_str());
    }
    catch (std::system_error &e) {
        BUILDBOX_LOG_ERROR("Could not create CAS directory structure in "
                           << d_storage_root);
        throw e;
    }
}

bool FsLocalCas::hasBlob(const Digest &digest)
{
    struct stat buf;
    const std::string path = filePath(digest);
    const int access_result = stat(path.c_str(), &buf);

    if (access_result != 0) {
        if (errno == ENOENT) { // Top directory and/or file do not exist.
            return false;
        }
        throw std::runtime_error("Could not verify whether " + path +
                                 "exists: " + std::string(strerror(errno)));
    }

    // If the file exists, we also make sure that the size reported in the
    // digest matches its size:
    if (S_ISREG(buf.st_mode)) {
        const long file_size = buf.st_size;

        if (file_size == digest.size_bytes()) {
            return true;
        }
        BUILDBOX_LOG_DEBUG(digest.hash()
                           << "/" << digest.size_bytes()
                           << ": file matches hash but not size: " << file_size
                           << " bytes");
    }

    return false;
}

void FsLocalCas::writeBlob(const Digest &digest, const std::string &data)
{
    validateDigest(digest, data);

    if (hasBlob(digest)) { // No need to perform the write.
        return;
    }

    // Creating a temp file and writing the blob data into it...
    buildboxcommon::TemporaryFile temp_file(d_temp_directory.c_str(),
                                            "cas-tmp");
    const std::string temp_filename(temp_file.name());

    std::ofstream file(temp_filename, std::fstream::binary);
    file << data;
    file.close();

    if (!file.good()) {
        const std::string error_message = "Failed writing to temporary file " +
                                          temp_filename + ": " +
                                          strerror(errno);
        BUILDBOX_LOG_ERROR(error_message);
        throw std::runtime_error(error_message);
    }

    // Getting the path where the blob should be stored in CAS.
    // If the parent subdirectory does not exist, it will be (atomically)
    // created:
    const std::string path_in_cas = filePath(digest, true);

    // Now we can make a hard link from the CAS directory to the
    // temporary file before it gets deleted. This has the effect of writing
    // the file atomically into the local CAS.
    // In the worst case, someone could beat us to placing another copy of
    // the blob in the CAS. But, because the contents will be the same, that is
    // not a problem.
    const int link_result = link(temp_filename.c_str(), path_in_cas.c_str());
    if (link_result != 0 && errno != EEXIST) {
        const std::string error_message =
            "Could not hard link temporary file " + temp_filename + ": " +
            strerror(errno);
        BUILDBOX_LOG_ERROR(error_message);
        throw std::runtime_error(error_message);
    }

    const int delete_temp_file_result = unlink(temp_filename.c_str());
    if (delete_temp_file_result != 0) {
        BUILDBOX_LOG_ERROR("Could not remove temporary file "
                           << temp_filename << ": " << strerror(errno));
    }
}

std::string FsLocalCas::readBlob(const Digest &digest, size_t offset,
                                 size_t length)
{
    const size_t data_size = static_cast<size_t>(digest.size_bytes());

    size_t bytes_to_read;
    if (length == npos) {
        bytes_to_read = std::max(size_t(0), data_size - offset);
    }
    else {
        bytes_to_read = length;
    }

    if (offset > data_size || (offset + bytes_to_read > data_size)) {
        throw std::out_of_range("The given interval is out of bounds.");
    }

    if (bytes_to_read == 0) {
        return "";
    }

    std::ifstream file = openFile(digest);
    // The file exists and is now open. (Otherwise, openFile() threw.)
    // The range that we want to read is inside the data.

    file.seekg(static_cast<std::streamoff>(offset), std::ios_base::beg);

    std::unique_ptr<char[]> buffer(new char[bytes_to_read + 1]);
    buffer[bytes_to_read] = '\0';

    file.read(buffer.get(), static_cast<std::streamsize>(bytes_to_read));
    if (file.good()) {
        return std::string(buffer.get());
    }

    const std::string error_message =
        "Error reading: " + filePath(digest) + ": " + strerror(errno);
    BUILDBOX_LOG_ERROR(error_message);
    throw std::runtime_error(error_message);
}

std::string FsLocalCas::readBlob(const Digest &digest)
{
    return readBlob(digest, 0, LocalCas::npos);
}

std::string FsLocalCas::path(const Digest &digest)
{
    if (!hasBlob(digest)) {
        throw std::invalid_argument("Digest is not stored in local CAS.");
    }

    return filePath(digest, false);
}

std::string FsLocalCas::filePath(const Digest &digest,
                                 bool create_parent_directory) const
{
    const std::string hash = digest.hash();
    const std::string directory_name = hash.substr(0, k_HASH_PREFIX_LENGTH);
    const std::string file_name = hash.substr(k_HASH_PREFIX_LENGTH);

    const std::string path_in_cas = d_objects_directory + "/" + directory_name;

    if (create_parent_directory) {
        buildboxcommon::FileUtils::create_directory(path_in_cas.c_str());
    }

    return path_in_cas + "/" + file_name;
}

void FsLocalCas::validateDigest(const Digest &digest,
                                const std::string &data) const
{
    const Digest computed_digest = buildboxcommon::CASHash::hash(data);

    if (digest == computed_digest) {
        return;
    }

    std::stringstream error_message;
    error_message << "Digest " << digest << " does not match data (expected "
                  << computed_digest << ")";

    throw std::invalid_argument(error_message.str());
}

std::ifstream FsLocalCas::openFile(const Digest &digest)
{
    const std::string path = filePath(digest);

    std::ifstream file;
    file.open(path, std::fstream::binary);

    if (file.fail()) {
        const std::string error_message =
            "Could not open file " + path + ": " + std::strerror(errno);
        BUILDBOX_LOG_ERROR(error_message);
        throw BlobNotFoundException(error_message);
    }

    return file;
}
