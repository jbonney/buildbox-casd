/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_DUMMYCAS_H
#define INCLUDED_BUILDBOXCASD_DUMMYCAS_H

#include <map>
#include <string>

#include <buildboxcasd_localcas.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {
namespace testing {

class DummyCas : public LocalCas {
    // Implements a dummy CAS used for testing purposes.
  public:
    bool hasBlob(const Digest &digest);

    void writeBlob(const Digest &digest, const std::string &data);

    std::string readBlob(const Digest &digest);

    std::string readBlob(const Digest &digest, size_t offset, size_t length);

    std::string path(const Digest &digest);

  private:
    std::map<std::string, std::string> d_data;
};

} // namespace testing
} // namespace buildboxcasd

#endif
