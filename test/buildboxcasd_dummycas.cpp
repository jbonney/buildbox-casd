/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_dummycas.h>

#include <sstream>

#include <buildboxcommon_cashash.h>

using namespace buildboxcasd::testing;

bool DummyCas::hasBlob(const Digest &digest)
{
    return d_data.count(digest.hash()) == 1;
}

void DummyCas::writeBlob(const Digest &digest, const std::string &data)
{
    if (digest == buildboxcommon::CASHash::hash(data)) {
        d_data[digest.hash()] = data;
    }
    else {
        throw std::invalid_argument("Digest given does not match data");
    }
}

std::string DummyCas::readBlob(const Digest &digest)
{
    try {
        return d_data.at(digest.hash());
    }
    catch (std::out_of_range &) {
        throw BlobNotFoundException("Digest is not in the DummyCAS");
    }
}

std::string DummyCas::readBlob(const Digest &digest, size_t offset,
                               size_t length)
{
    const auto data = readBlob(digest);

    return data.substr(offset, length);
}

std::string DummyCas::path(const Digest &digest) { return ""; }
