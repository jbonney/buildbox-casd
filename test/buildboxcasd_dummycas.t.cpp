/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_dummycas.h>

#include <gtest/gtest.h>

#include <buildboxcommon_protos.h>

using buildboxcommon::Digest;

class DummyCasTest : public ::testing::Test {
  protected:
    buildboxcasd::testing::DummyCas cas;
};

inline static Digest make_digest(const std::string &data)
{
    return buildboxcommon::CASHash::hash(data);
}

TEST_F(DummyCasTest, Empty)
{
    const Digest digest = make_digest("SomeData");
    ASSERT_FALSE(cas.hasBlob(digest));
}

TEST_F(DummyCasTest, Insert)
{
    const std::string data = "data1";
    const Digest digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));
    cas.writeBlob(digest, data);
    ASSERT_TRUE(cas.hasBlob(digest));
}

TEST_F(DummyCasTest, WriteWithInvalidDigestThrows)
{
    const std::string data = "nonZeroLengthData";
    Digest digest = make_digest(data);
    digest.set_size_bytes(0);

    ASSERT_THROW(cas.writeBlob(digest, "nonZeroLengthData"),
                 std::invalid_argument);
}

TEST_F(DummyCasTest, Read)
{
    const std::string data = "DataForHash44";
    const Digest digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);
}

TEST_F(DummyCasTest, ReadUntilEndLimit)
{
    const std::string data = "ThisIsSomeData...";
    const Digest digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest, 0, buildboxcasd::LocalCas::npos), data);
}

TEST_F(DummyCasTest, ReadWithOffsetAndLimit)
{
    const std::string data = "ThisIsSomeData";
    const Digest digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest, 4, 2), "Is");
}

TEST_F(DummyCasTest, ReadWithInvalidOffset)
{
    const std::string data = "data";
    const Digest digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_THROW(cas.readBlob(digest, 5, 4), std::out_of_range);
}

TEST_F(DummyCasTest, ReadLimitZero)
{
    const std::string data = "data123";
    const Digest digest = make_digest(data);

    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 4, 0), "");
}

TEST_F(DummyCasTest, ReadLimitZeroWithInvalidOffset)
{
    const std::string data = "data";
    const Digest digest = make_digest(data);

    cas.writeBlob(digest, data);

    ASSERT_THROW(cas.readBlob(digest, 100, 0), std::out_of_range);
}

TEST_F(DummyCasTest, ReadMissingBlobThrows)
{
    const Digest digest = make_digest("ThisDataIsNotInCAS");
    ASSERT_FALSE(cas.hasBlob(digest));
    ASSERT_THROW(cas.readBlob(digest), buildboxcasd::BlobNotFoundException);
}

TEST_F(DummyCasTest, PathIsEmpty)
{
    // The dummy CAS doesn't store blobs on disk, so it returns empty paths.
    Digest digest = make_digest("1234");
    cas.writeBlob(digest, "1234");

    ASSERT_TRUE(cas.path(digest).empty());
    ASSERT_TRUE(cas.path(digest).empty());
}
