﻿/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcas.h>

#include <fstream>
#include <unistd.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_temporarydirectory.h>

#include <gtest/gtest.h>

using namespace buildboxcasd;

class LocalCasFixture : public ::testing::Test {
  protected:
    LocalCasFixture() : cas(cas_root_directory.name()) {}

    buildboxcommon::TemporaryDirectory cas_root_directory;
    FsLocalCas cas;
};

inline static Digest make_digest(const std::string &data)
{
    return buildboxcommon::CASHash::hash(data);
}

TEST_F(LocalCasFixture, EmptyCasDirectoryStructure)
{
    const auto cas_objects_path =
        std::string(cas_root_directory.name()) + "/cas/objects/";
    const auto temp_objects_path =
        std::string(cas_root_directory.name()) + "/cas/tmp/";

    ASSERT_EQ(access(cas_objects_path.c_str(), F_OK), 0);
    ASSERT_EQ(access(temp_objects_path.c_str(), F_OK), 0);
}

TEST_F(LocalCasFixture, TestEmpty)
{
    ASSERT_FALSE(cas.hasBlob(make_digest("data")));
}

TEST_F(LocalCasFixture, WriteAndReadEmptyBlob)
{
    Digest digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), "");
}

TEST_F(LocalCasFixture, WriteWithInvalidHash)
{
    const std::string data = "data";
    Digest digest = make_digest(data);

    digest.set_hash(digest.hash() + "a");

    ASSERT_THROW(cas.writeBlob(digest, data), std::invalid_argument);
}

TEST_F(LocalCasFixture, WriteWithInvalidSize)
{
    const std::string data = "data";
    Digest digest = make_digest(data);

    digest.set_size_bytes(digest.size_bytes() + 1);

    const auto invalid_data_size =
        static_cast<google::protobuf::int64>(data.size() + 1);
    digest.set_size_bytes(invalid_data_size);

    ASSERT_THROW(cas.writeBlob(digest, data), std::invalid_argument);
}

TEST_F(LocalCasFixture, TestWriteAndRead)
{
    const std::string data = "someData";
    const auto digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    // Data was written and is valid:
    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);
    ASSERT_EQ(cas.readBlob(digest, 0, static_cast<size_t>(data.size())), data);
}

TEST_F(LocalCasFixture, ReadMissingBlobThrows)
{
    const auto digest = make_digest("dataNotPresent");
    ASSERT_FALSE(cas.hasBlob(digest));
    ASSERT_THROW(cas.readBlob(digest), BlobNotFoundException);
}

TEST_F(LocalCasFixture, ReadEmptyBlob)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_EQ(cas.readBlob(digest, 0, LocalCas::npos), "");
}

TEST_F(LocalCasFixture, ReadEmptyBlobWithZeroLength)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_EQ(cas.readBlob(digest, 0, 0), "");
}

TEST_F(LocalCasFixture, ReadEmptyBlobWithPositiveOffsetAndZeroLength)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_THROW(cas.readBlob(digest, 1, 0), std::out_of_range);
}

TEST_F(LocalCasFixture, ReadEmptyBlobWithPositiveOffsetAndNposLength)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_THROW(cas.readBlob(digest, 1, LocalCas::npos), std::out_of_range);
}

TEST_F(LocalCasFixture, EmptyRead)
{
    const auto data = "alpha";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 1, 0), "");
}

TEST_F(LocalCasFixture, PartialRead)
{
    const auto data = "alpha bravo charlie";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 6, 5), "bravo");
}

TEST_F(LocalCasFixture, InvalidOffsetThrows)
{
    const auto data = "123";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_THROW(cas.readBlob(digest, 4, 1), std::out_of_range);
}

TEST_F(LocalCasFixture, ReadingUntilEndFromDataSize)
{
    const std::string data = "abcde";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, data.size(), LocalCas::npos), "");
}

TEST_F(LocalCasFixture, ExcessiveLengthThrows)
{
    const std::string data = "abcde";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_THROW(cas.readBlob(digest, 1, static_cast<size_t>(data.size())),
                 std::out_of_range);
}

TEST_F(LocalCasFixture, NposBytesReadsUntilEnd)
{
    const auto data = "-abcdefghi";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 1, buildboxcasd::LocalCas::npos),
              "abcdefghi");
}

TEST_F(LocalCasFixture, TestWriteOutputs)
{
    const auto data = "data123";
    const auto digest = make_digest(data);
    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    // Checking that the physical file is where we expect it:
    const auto cas_objects_path =
        std::string(cas_root_directory.name()) + "/cas/objects";
    const auto parent_subdirectory_name = digest.hash().substr(0, 2);
    const auto filename = digest.hash().substr(2);

    const auto expected_file_path =
        cas_objects_path + "/" + parent_subdirectory_name + "/" + filename;

    // File exists:
    ASSERT_EQ(access(expected_file_path.c_str(), F_OK), 0);

    // And the data stored in it is what we wrote:
    std::ifstream file(expected_file_path);
    ASSERT_TRUE(file.good());

    std::stringstream file_contents;
    file_contents << file.rdbuf();
    ASSERT_TRUE(file_contents.good());

    ASSERT_EQ(file_contents.str(), data);
}

TEST_F(LocalCasFixture, GetPath)
{
    const auto data = "data";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    const size_t hash_prefix_length =
        static_cast<size_t>(cas.pathHashPrefixLength());

    const std::string expected_path =
        std::string(cas_root_directory.name()) + "/cas/objects/" +
        digest.hash().substr(0, hash_prefix_length) + "/" +
        digest.hash().substr(hash_prefix_length, std::string::npos);

    ASSERT_EQ(cas.path(digest), expected_path);
}

TEST_F(LocalCasFixture, GetPathForBlobNotPresentThrows)
{
    const auto digest = make_digest("Data not present in the LocalCAS.");
    ASSERT_FALSE(cas.hasBlob(digest));
    ASSERT_THROW(cas.path(digest), std::invalid_argument);
}
